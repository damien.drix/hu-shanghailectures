
from ctrnn import update_ctrnn
import numpy as np



from nxt.bluesock import BlueSock
from nxt.sensor import *
from nxt.motor import *


if __name__ == "__main__":
    # Connect to the brick:
    s = BlueSock("00:16:53:12:34:2D")
    # s = BlueSock("00:16:53:10:C6:8F")
    b = s.connect()
    print b

    # Setup the motors
    motor_left = Motor(b, PORT_A)
    motor_right = Motor(b, PORT_B)

    def normalise_speed(val, low=0.0, vmin=64, vmax=120):
        if val <= low: return 0
        return min(vmax, max(vmin, vmin + val * (vmax - vmin)))


    # Setup the sensors
    sensors = "light"
    if sensors =="ultrasound":
        sensor_left= Ultrasonic(b, 0)
        sensor_left.command(Ultrasonic.Commands.CONTINUOUS_MEASUREMENT)
        sensor_left.set_interval((0.0,))

        sensor_right= Ultrasonic(b, 1)
        sensor_right.command(Ultrasonic.Commands.CONTINUOUS_MEASUREMENT)
        sensor_right.set_interval((0.0,))

        vmin = 0
        vmax = 120
        def normalise_sensor(val):
            # Compute a proximity signal from the raw distance value:
            return 1 - min(1.0, max(0.0, (val - vmin) / float(vmax - vmin)))    

    elif sensors == "light":
        sensor_left= Light(b, 0)
        sensor_left.set_illuminated(False)
        
        sensor_right= Light(b, 1)
        sensor_right.set_illuminated(False)

        vmin=200
        vmax=600
        def normalise_sensor(val):
            # Compute a proximity signal from the raw light value:
            return min(1.0, max(0.0, (val - vmin) / float(vmax - vmin)))

    # Setup a CTRNN network:
    dt = 0.01
    n = 5
    bias = np.array([-1., -1., -6., -10., -10.])
    tau = np.array([.2, .2, 3., .2, .2])
    y = np.array([1., 1., 5.5, 10., 10.])
    weights = np.zeros((n, n))
    weights[0,2] = 1.5
    weights[1,2] = 1.5
    weights[2,2] = 12.0
    weights[1,3] = 4.0
    weights[2,3] = 4.0
    weights[3,3] = 7.0
    weights[0,4] = 4.0
    weights[2,4] = 4.0
    weights[4,4] = 7.0

    interval = 0.1

    # Main loop
    try:
        while True:
            # Measure the sensory signals
            left_act = normalise_sensor(sensor_left.get_sample())
            right_act = normalise_sensor(sensor_right.get_sample())
            print "#", left_act, right_act

            t = time.time()

            # Simulate the CTRNN network for a short amount of time:
            x = np.array([left_act, right_act, 0, 0, 0])
            for _ in xrange(200):
                sigma, y = update_ctrnn(x, y, weights, bias, tau, dt)

            # Wait in case we were too fast:
            elapsed = time.time() - t
            time.sleep(max(0, interval - elapsed))

            # Compute the motor commands
            left_speed = normalise_speed(sigma[3] * 100 - 1.2)
            right_speed = normalise_speed(sigma[4] * 100 - 1.2)
            print "@", left_speed, right_speed

            # Adjust the motor speeds
            motor_left.run(left_speed)
            motor_right.run(right_speed)

    finally:
        # Stop the motors
        motor_left.idle()
        motor_right.idle()



