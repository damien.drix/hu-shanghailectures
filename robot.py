
from nxt.bluesock import BlueSock
from nxt.sensor import *
from nxt.motor import *


if __name__ == "__main__":
    # Connect to the brick:
    s = BlueSock("00:16:53:12:34:2D")
    b = s.connect()
    print b

    # Setup the motors
    motor_left = Motor(b, PORT_A)
    motor_right = Motor(b, PORT_B)

    def normalise_speed(val, low=0.0, vmin=64, vmax=120):
        if val <= low: return 0
        return min(vmax, max(vmin, vmin + val * (vmax - vmin)))


    # Setup the sensors
    sensors = "ultrasound"
    if sensors =="ultrasound":
        sensor_left= Ultrasonic(b, 0)
        sensor_left.command(Ultrasonic.Commands.CONTINUOUS_MEASUREMENT)
        sensor_left.set_interval((0.0,))

        sensor_right= Ultrasonic(b, 1)
        sensor_right.command(Ultrasonic.Commands.CONTINUOUS_MEASUREMENT)
        sensor_right.set_interval((0.0,))

        vmin = 0
        vmax = 80
        def normalise_sensor(val):
            # Compute a proximity signal from the raw distance value:
            return 1 - min(1.0, max(0.0, (val - vmin) / float(vmax - vmin)))    

    elif sensors == "light":
        sensor_left= Light(b, 0)
        sensor_left.set_illuminated(False)
        
        sensor_right= Light(b, 1)
        sensor_right.set_illuminated(False)

        vmin=200
        vmax=1024
        def normalise_sensor(val):
            # Compute a proximity signal from the raw light value:
            return min(1.0, max(0.0, (val - vmin) / float(vmax - vmin)))

    # Set the Braitenberg configuration
    mode = "aggressor"
    if mode == "coward":
        left_bias = -0.2
        right_bias = -0.2

        left_left = 1.0
        left_right = 0.0
        right_left = 0.0
        right_right = 1.0

    elif mode == "aggressor":
        left_bias = 0.0
        right_bias = 0.0

        left_left = 0.0
        left_right = 1.0
        right_left = 1.0
        right_right = 0.0

    elif mode == "lover":
        # modify here:
        left_bias = 0.0
        right_bias = 0.0
        
        left_left = 0.0
        left_right = 0.0
        right_left = 0.0
        right_right = 0.0

    elif mode == "explorer":
        # modify here:
        left_bias = 0.0
        right_bias = 0.0
        
        left_left = 0.0
        left_right = 0.0
        right_left = 0.0
        right_right = 0.0

    # Main loop
    try:
        while True:
            # Measure the sensory signals
            left_act = normalise_sensor(sensor_left.get_sample())
            right_act = normalise_sensor(sensor_right.get_sample())
            print "#", left_act, right_act

            # Compute the motor commands
            left_speed = normalise_speed(left_bias + left_act * left_left + right_act * right_left)
            right_speed = normalise_speed(right_bias + left_act * left_right + right_act * right_right)
            print "@", left_speed, right_speed

            # Adjust the motor speeds
            motor_left.run(left_speed)
            motor_right.run(right_speed)

    finally:
        # Stop the motors
        motor_left.idle()
        motor_right.idle()


