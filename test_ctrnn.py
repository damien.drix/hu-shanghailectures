
from ctrnn import update_ctrnn
import pylab as plt
import numpy as np


def run_network():
	N = 2

	weights = np.zeros((N,N))
	bias = np.zeros(N)
	tau = np.ones(N)

	# Weights for limit cycle:
	# weights[0,0] = 4.5
	# weights[0,1] = -1.0
	# weights[1,0] = 1.0
	# weights[1,1] = 4.5

	# Weights for point attractor:
	weights[0,0] = 3
	weights[0,1] = -1.0
	weights[1,0] = 1.0
	weights[1,1] = 3

	 # Set the bias values:
	bias[0] = -2.75
	bias[1] = -1.75

	# Set the time constants:
	tau[:] = 1.0
	
	# Set the integration timestep to a small value
	# with respect to the smallest time constant:
	dt = 0.1

	y = np.random.uniform(low=-1.0, high=1.0, size=(N,))
	x = np.zeros(N)

	ts = np.arange(0, 120, dt)
	ys = np.zeros((N, ts.size))

	for i,t in enumerate(ts):
		sigma, y = update_ctrnn(x, y, weights, bias, tau, dt)
		ys[:,i] = y

	return ts, ys


if __name__ == "__main__":
	plt.figure()
	
	plt.xlabel("t")
	plt.ylabel("y")
	
	# plt.xlabel("y1")
	# plt.ylabel("y2")

	for _ in range(10):
		ts, ys = run_network()
		plt.plot(ts, ys[0])
		plt.plot(ts, ys[1])
		# plt.plot(ys[0], ys[1])

	plt.show()


