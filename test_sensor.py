from nxt.bluesock import BlueSock
from nxt.sensor import *
from nxt.motor import *
import time


if __name__ == "__main__":
    # Connect to the brick:
    s = BlueSock("00:16:53:12:34:2D")
    # s = BlueSock("00:16:53:10:C6:8F")
    b = s.connect()
    print b

    # sensor_left= Ultrasonic(b, 0)
    # sensor_left.command(Ultrasonic.Commands.CONTINUOUS_MEASUREMENT)
    # sensor_left.set_interval((0.1,))

    sensor_left= Light(b, 0)
    sensor_left.set_illuminated(False)

    while True:
    	print sensor_left.get_sample()

