import random
import numpy as np


def mutate(genome, n=1, sigma=0.2):
	""" Mutates a genome.

	Arguments
	----------
	genome: a sequence of genes (mutated in-place).
	n: average number of mutations.
	sigma: standard deviation of the gaussian drift.

	"""
	# Find the mutation probability from the average number of mutations:
	p = n / float(len(genome))

	# Iterate through the genome and randomly
	# mutate some loci with probability p:
	for i in xrange(len(genome)):
		if np.random.uniform() < p:
			genome[i] += np.random.normal(scale=sigma) # gaussian drift

	# Constrain the values to the [0.0, 1.0] interval:
	np.clip(genome, 0.0, 1.0, out=genome)


def crossover(receiver, donor, p=0.5):
	""" Performs a random crossover of two genomes.

	Arguments
	----------
	receiver: a sequence of genes (modified in-place)
	donor: a sequence of genes (not modified)
	p: the probability of copying each locus from
	   the donor to the receiver (default: 0.5)

	"""
	for i in xrange(len(genome)):
		if np.random.uniform() < p:
			receiver[i] = donor[i]


def tournament(a, b, fitness):
	""" Ranks two genomes according to the supplied fitness function. """
	if fitness(a) > fitness(b):
		return a, b
	else:
		return b, a


def iteration(population, fitness):
	""" Performs one iteration of the steady-state GA. """
	# Pick two individuals
	a, b = random.sample(population, 2)

	# Stage a tournament:
	winner, loser = tournament(a, b, fitness)

	# Infect and mutate the loser:
	crossover(loser, winner)
	mutate(loser)

