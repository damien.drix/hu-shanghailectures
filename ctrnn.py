"""
A simple continuous-time recurrent neural network (CTRNN) implemented using numpy.
"""

import numpy as np


def update_ctrnn(x, y, weights, bias, tau, dt):
	"""
	Updates a CTRNN system using Euler integration.

	Parameters
	------------
	x: input vector (N) at time t
	y: state vector (N) at time t
	weights: weights matrix (N*N) indexed as weights[to,from]
	bias: bias vector (N)
	tau: time constants vector (N)
	dt: integration timestep

	Return values
	----------------
	sigma: activation vector (N) at time t+dt
	y: updated state vector (N) at time t+dt

	"""
	sigma = 1 / (1.0 + np.exp(-(y + bias)))
	dy = (weights.T.dot(sigma) + x - y) / tau
	y += dy * dt

	return sigma, y


