"""
Functions for encoding and decoding CTRNN genomes.
"""

import numpy as np


def normalise(values, (vmin, vmax)):
	return (values - vmin) / (vmax - vmin)


def denormalise(values, (vmin, vmax)):
	return values * (vmax - vmin) + vmin


def ctrnn_codec(n, weight_range, tau_range):
	"""
	Generates a genome encoder and decoder for a CTRNN system.

	Arguments
	----------
	n: the number of neurons.
	weight_range: a (min, max) tuple indicating the value range
	              for the weights and bias.
	tau_range: a (min, max) tuple indicating the value range
	           for the time constants.

	Returns
	---------
	encoder: an encoding function(tau, bias, weights) -> genome
	decoder: a decoding function(genome) -> tau, bias, weights

	"""
	def encode(tau, bias, weights):
		return np.concatenate(
			normalise(tau, tau_range),
			normalise(bias, weight_range),
			normalise(weights.flatten(), weight_range))

	def decode(genes):
		return (denormalise(genes[:n], tau_range),
				denormalise(genes[n:2*n], weight_range), 
				denormalise(genes[2*n:].reshape((n,n)), weight_range))

	return encode, decode